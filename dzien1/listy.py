# 1. (map) Napisz funkcję, która przyjmuje listę liczb i zwraca listę
# liczb 2 razy większych np.: [1,2,3] -> [2,4,6]
# a) wersja z petla
# b) wersja z list comprehension




# 2. (filter) Napisz funkcję, która przyjmuje listę i filtruje z tej listy wyłącznie liczby
# całkowite np.: [1, 'tomek', 3.3, 4] -> [1, 4]
# isinstance(a, int)
# a) wersja z petla
# b) wersja z list comprehension




# 3. Napisz funkcje laczaca oba powyzsze podpunkty
# np.: [1, 'tomek', 3.3, 4] -> [2, 8]
# a) wersja z petla
# b) wersja wykorzystujaca wczesniej zdefiniowane funkcje
# c) wersja z list comprehension





# 4. (reduce) Napisz funkcje, wyliczajaca srednia arytmetyczna z listy liczb calkowitych
# a) wersja z pętlą
# b) sum()




# 5. napisz funkcję, wyliczajaca srednia arytmetyczną z wartosci bezwzglednych
# liczb calkowitych zawartych w liscie (wejsciowa lista moze zawierac rozne dane)
# np [-1, 2.2, 'tomek', 1] -> 1
# a) wersja korzystajaca z funkcji wyzej
# b) wersja z pętlą

























lista_liczb = list(range(1,21))

print(lista_liczb)

nowa_lista = []
for i in lista_liczb:
    if i % 2 == 0:
        nowa_lista.append(i*2)

print(nowa_lista)


nowa_lista = [i*2 for i in lista_liczb if i % 2 == 0]
print(nowa_lista)





