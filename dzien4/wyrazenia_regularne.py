import re

'''
\d - Grupa liczbowa synonim [0-9]
\w - Grupa “słowna” - synonim [a-zA-Z0-9_]
* - zero lub wiecej
+ - jeden lub wiecej
'''

# wynik_match = re.match(r"(\d+)\.(\d+)", "abc24.1632def")
# print(wynik_match)

# wynik_match = re.match(r"(\d+)\.(\d+)", "24.1632")
# print(wynik_match)

# wynik_match = re.match(r"(\d+)\.(\d+)", "241632")
# print(wynik_match)


# wynik_match = re.match(r"a[bcd]*b", "abcdbcdb abcdbcdb")
# print(wynik_match.group())

#
# wynik_match = re.match(r"a[bcd]*b", "bcdbcdb bbbbab")
# print(wynik_match)

# pierwsze_polozenie = re.search('(?<=abc)def', 'abcdef')
# print(pierwsze_polozenie)

# #
# pierwsze_polozenie = re.search(r"(\d+)\.(\d+)", "abc24.1632def")
# print(pierwsze_polozenie)
# print(pierwsze_polozenie.group())
# #
# wszystkie_wystapienia_lista = re.findall('[a-zA-Z]+', "Atom B 99")
# print(wszystkie_wystapienia_lista)
#
wszystkie_wystapienia_iterator = re.finditer('\w+', "Atom B")
print(wszystkie_wystapienia_iterator)
for i in wszystkie_wystapienia_iterator:
    print(i.group())