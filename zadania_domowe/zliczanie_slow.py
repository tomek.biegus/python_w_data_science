with open('./dziela_Mickiewicza.txt', encoding="utf8") as f:
    lista = [] #utworzenie listy, do której wrzucimy każde słowo z pliku tekstowego
    for linia in f:
        linia = linia.lower()
        lista += linia.split()
    zbior = list(set(lista)) #uworzenie zbioru z unikatowymi wyrazami z tekstu
    slownik = {} #utworzenie słownika, w którym kluczem będzie unikatowy wyraz, a wartością początkową 0
    for i in zbior:
        slownik[i] = 0
    for i in lista:
        slownik[i] +=1 #pętla, w której dla każdego wyrazu w liście zmieniamy wartość słownika na +1


    def najczestsze_slowa(slownik,liczba_slow):
        lista_slow=[]
        i = 1
        while i <= liczba_slow:
            wartosc_maks = 1
            for k,v in slownik.items():
                if wartosc_maks < v and len(k)>1: #istotne są tylko słowa, które mają więcej niż jedną literę
                    wartosc_maks = v
                    klucz = k
                    krotka = (k,v)
            del slownik[klucz]
            lista_slow += krotka
            i += 1
        return lista_slow


    def najczestsze_slowa_2(slownik, liczba_slow):
        lista_par = list(slownik.items())
        posortowane_pary = sorted(
            lista_par,
            key=lambda krotka: krotka[1],
            reverse=True
        )
        najczestsze_slowa = posortowane_pary[:liczba_slow]
        return najczestsze_slowa





# print(najczestsze_slowa(slownik,4))
print(najczestsze_slowa_2(slownik, 3))


#
# d = {'one':1,'three':3,'five':5,'two':2,'four':4}
# # 1
# # a = sorted(d.items(), key=lambda x: x[1])
# # 2
# # a = list(d.items())
# # a.sort(key=lambda x: x[1])
# # 3
# a = sorted(d, key=d.get)
# print(a)



lista_list = [
    [1,2,3,4,5,5,5,5,5],
    [1,2,3,4,6],
    [1,2,3,2,3,4,5,5,5,5,5,5,5,5,5,5,5,5,5],
    [1,2,3,4,55555],
    [1,2,3,4,5,5,5],
]


def klucz(lista):
    return sum(lista)

klucz = lambda lista: len(lista)

posortowana_lista_list = sorted(
    lista_list,
    key=lambda x: sum(x),
    reverse=True
)

print(posortowana_lista_list)

print(type(klucz))
