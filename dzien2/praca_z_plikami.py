# gorszy sposob otwierania pliku tekstowego (trzba pamietac o plik.close(), lepiej uzyc managera kontekstu with):
# plik = open('dane.txt')

#sposoby czytania z pliku tekstowego
# sposob 1
# print(plik.readline(), end='')
# print(plik.readline(), end='')

# sposob 2
# linie = plik.readlines()
# print(type(linie))
# print(linie)

# sposob 3 (najlepszy
# for linia in plik:
#     print(linia, end='')


# lepszy sposob otwierania pliku tekstowego:

# manager kontekstu
# with open('dane.txt', 'rt') as plik:
#     for linia in plik:
#         print(linia)


# zapisywanie do pliku
# with open('dane_zapis.txt', 'wt') as plik:
#     plik.write('nowe rzeczy')



