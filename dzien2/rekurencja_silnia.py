def silnia_iteracyjnie(n):
    silnia = 1
    for i in range(2,n+1):
        silnia = silnia * i
    return silnia


def silnia_rekurencyjnie(n):
    if n == 1:
        return 1
    else:
        return n * silnia_rekurencyjnie(n-1)


def silnia_while(n):
    silnia = 1
    while n > 0:
        silnia *= n
        n -= 1
    return silnia


if __name__ == "__main__":
    print(silnia_iteracyjnie(5))
    print(silnia_rekurencyjnie(5))
    print(silnia_while(5))




