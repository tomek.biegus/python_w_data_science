def wyswietl_cechy_pralki(
        pojemnosc, # argument poycyjny 1
        zuzycie_elektrycznosci, # argument pozycyjny 2
        liczba_programow=4, # argument nazwany 1
        kolor='bialy' # argument nazwany 2
):
    print(f"{pojemnosc}, {zuzycie_elektrycznosci}, {liczba_programow}, {kolor}")

wyswietl_cechy_pralki(30, 4, liczba_programow=3)



def wypisz_wszystko(*args, **kwargs):
    print(args)
    for a in args:
        print(a, end=' ')
    print()
    print(kwargs)
    print(type(kwargs))
    print()

argumenty = (7,6,5)
wypisz_wszystko(*argumenty, 'tomek')
wypisz_wszystko(1, 2, 3, 4, 5, 6)
wypisz_wszystko('tomek', 'marek', wiek=19, kolor_oczu='niebieskie')
