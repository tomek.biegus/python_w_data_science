def czy_pierwsza(x: int) -> bool:
    '''zakladamy ze uzytkownik podaje
    liczbe calkowita, wieksza od 0
    '''
    if x == 1:
        return False
    for potencjalny_dzielnik in range(2, x):
        if x % potencjalny_dzielnik == 0:
            return False
    return True


print(czy_pierwsza(1))