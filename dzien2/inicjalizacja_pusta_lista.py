# Źle:
def zmien_liste(lista=[]):
    lista.append(5)
    print(lista)

print("błędnie:")
zmien_liste()
zmien_liste()
zmien_liste()
zmien_liste()


# Poprawnie:
def zmien_liste(lista=None):
    if lista is None:
        lista = [5]
    else:
        lista.append(5)
    print(lista)

print("poprawnie:")
zmien_liste()
zmien_liste()
zmien_liste()
zmien_liste()