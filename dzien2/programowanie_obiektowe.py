class Samochod:
    przyspieszenie = 10

    def __init__(self, numer_rejestracyjny):
        self.numer_rejestracyjny = numer_rejestracyjny
        self.czy_w_ruchu = False
        self.predkosc_km_h = 0

    def podaj_predkosc_m_s(self):
        return self.predkosc_km_h / 3.6

    def zmien_predkosc_km_h(self, nowa_predkosc):
        self.predkosc_km_h = nowa_predkosc
        if nowa_predkosc != 0:
            self.czy_w_ruchu = True
        else:
            self.czy_w_ruchu = False

    def czy_przekroczono_predkosc_maksymalna(self):
        return self.predkosc_km_h > 140


class SamochodSpalinowy(Samochod):

    def __init__(self, numer_rejestracyjny, pojemnosc_baku):
        super().__init__(numer_rejestracyjny)
        self.pojemnosc_baku = pojemnosc_baku

    def czy_przekroczono_predkosc_maksymalna(self):
        return self.predkosc_km_h > 100


if __name__ == "__main__":
    # tworzymy obiekt klasy Samochod
    moj_maluch = SamochodSpalinowy('1234', pojemnosc_baku=80)
    # mamy dostep do jego atrybutow
    print(moj_maluch.pojemnosc_baku)

    # Ponizej przyklad ze bezposredni dostep do atrybutow klasy
    # moze skutkowac problemami, mozemy zmienic predkosc na niezerowa a zapomniec
    # ustawic atrybut czy_w_ruchu na True
    # poprawnie zajmuje sie tym metoda zmien_predkosc_km_h
    print(moj_maluch.predkosc_km_h)
    moj_maluch.zmien_predkosc_km_h(190)
    print(moj_maluch.czy_w_ruchu)
    moj_maluch.zmien_predkosc_km_h(0)
    print(moj_maluch.czy_w_ruchu)
    moj_maluch.predkosc_km_h = 30
    print(moj_maluch.predkosc_km_h)
    print(moj_maluch.czy_w_ruchu)
    print()
    # ponizej przyklad nadpisywania metod
    # klasa Samochod i klasa SamochodSpalinowy maja
    # metode czy_przekroczono_predkosc_maksymalna,
    # klasa SamochodSpalinowy dziedziczy po klasie Samochod tę metodę,
    # jednak w klasie SamochodSpalinowy my ją celowo nadpisujemy = modyfikujemy
    # i teraz w zależności czy utrozyliśmy obiekt typu Samochod czy Samochod Spalinowy
    # w wyniku dzialania metody o tej samej nazwie ale wywolanej na rzecz
    # róznych obiektow dostajemy różne wyniki
    moj_samochod = Samochod('aaa')
    moj_samochod.zmien_predkosc_km_h(120)
    moj_samochod_spalinowy = SamochodSpalinowy('bbb', pojemnosc_baku=90)
    moj_samochod_spalinowy.zmien_predkosc_km_h(120)
    print(moj_samochod.czy_przekroczono_predkosc_maksymalna())
    print(moj_samochod_spalinowy.czy_przekroczono_predkosc_maksymalna())



