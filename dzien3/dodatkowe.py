import time


def say_hello(name):
    y = 2+5
    print(f'Siemka {name}, wynik {y}')

def my_decorator(f):
    def wrapper(imie):
        start = time.time()
        print("Uruchamiam funkcje")
        f(imie)
        print("funkcja wykonala sie poprawnie")
        stop = time.time()
        czas = stop - start
        print(f'Czas wykonania funkcji: {czas}')
    return wrapper


# dwa ponizsze zapisy sa na potrzeby zrozumienia idei
# mozna tak:
udekorowana_funkcja = my_decorator(say_hello)
udekorowana_funkcja("Roman")

# mozna tez tak (równoważnie):
my_decorator(say_hello)("tomek")


# w praktyce stosowany jest ponizszy zapis
@my_decorator
def say_goodbye(name):
    y = 2+5
    for i in range(10000000):
        y+=1
    print(f'Goodbye {name}, wynik {y}')


say_goodbye("Maciek")


