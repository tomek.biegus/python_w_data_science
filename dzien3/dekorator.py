
def wyswietl_szczegoly(f):
    def wrapper(*args, **kwargs):
        nazwa_funkcji = f.__name__
        print(f'przekazana do dekoratora funkcja nazywa sie {nazwa_funkcji}')
        print(f'argumenty przkazane do funkcji:')
        print('argumenty pozycyjne')
        for argument_pozycyjny in args:
            print(argument_pozycyjny, end=', ')
        print('\nnazwane argumenty:')
        for key, value in kwargs.items():
            print(f'{key} = {value}', end=', ')
        print()
        return f(*args, **kwargs)
    return wrapper


@wyswietl_szczegoly
def jakas_funkcja(imie, nazwisko, wiek=4):
    return 44

# wyswietl_szczegoly(jakas_funkcja)

print(jakas_funkcja("tomek", "biegus", wiek=33))

