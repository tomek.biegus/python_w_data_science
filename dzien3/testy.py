import unittest
import Lew

class Testing(unittest.TestCase):
    def test_string(self):
        simba = Lew(40, 'mufasa')
        simba.obetnij_grzywe()
        dlugosc_grzywy = simba.grzywa
        self.assertEqual(dlugosc_grzywy, 0)

    def test_boolean(self):
        a = True
        b = True
        self.assertEqual(a, b)

if __name__ == '__main__':
    unittest.main()