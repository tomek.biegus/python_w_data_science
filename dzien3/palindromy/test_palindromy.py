import unittest
from palindromy import is_palindrome


class MyTestCase(unittest.TestCase):

    def test_abba(self):
        slowo = 'abba'
        wynik = is_palindrome(slowo)
        self.assertTrue(wynik)  # add assertion here

    def test_abkba(self):
        slowo = 'abkba'
        wynik = is_palindrome(slowo)
        self.assertTrue(wynik)  # add assertion here

    def test_abcia(self):
        slowo = 'abcia'
        wynik = is_palindrome(slowo)
        self.assertFalse(wynik)  # add assertion here


    def test_mananame(self):
        slowo = 'mananame'
        wynik = is_palindrome(slowo)
        self.assertFalse(wynik)  # add assertion here

    def test_mananamet(self):
        slowo = 'mananamet'
        wynik = is_palindrome(slowo)
        self.assertFalse(wynik)  # add assertion here

    def test_mananamet_(self):
        slowo = 'mananamet '
        wynik = is_palindrome(slowo)
        self.assertFalse(wynik)  # add assertion here

    def test_mananamet_(self):
        slowo = 'man a nam'
        wynik = is_palindrome(slowo)
        self.assertTrue(wynik)  # add assertion here


if __name__ == '__main__':
    unittest.main()
